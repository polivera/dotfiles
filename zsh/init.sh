# Init zsh file
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DOTFILES_HOME=$( cd "$( dirname "$(readlink -f $0)" )/../" && pwd -P )

# User configuration
source ${XDG_DOTFILES_HOME}/zsh/oh-my-zsh.sh

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Custom configurations
source ${XDG_DOTFILES_HOME}/zsh/custom-zsh.sh

# Unset some vars
unset ZSH_CUSTOM_CONFIG
