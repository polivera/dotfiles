#!/usr/bin/env zsh
# ----------------------------------------------------------------------------------------------------------------
#  CUSTOM ZSH CONFIGURATION FILE
# ----------------------------------------------------------------------------------------------------------------

# Make console ignore ctrl-s and ctrl-q commands
stty -ixon

# ----------------------------------------------------------------------------------------------------------------
if [ -z ${TERM} ] && [ "$TERM" = "linux" ]; then
    export TERM=xterm-256color
fi

# ----------------------------------------------------------------------------------------------------------------
# Aliases
# ----------------------------------------------------------------------------------------------------------------
alias ls='ls --color'
alias ll='ls -l'
alias lla='ls -la'

# ----------------------------------------------------------------------------------------------------------------
# Include configs
# ----------------------------------------------------------------------------------------------------------------

#Bins
# source ${ZSH_CUSTOM_CONFIG}/includes/zsh.bins.sh
# SSH
#source ${ZSH_CUSTOM_CONFIG}/includes/zsh.ssh.sh
# Vim
#source ${ZSH_CUSTOM_CONFIG}/includes/zsh.vim.sh
# Java
#source ${ZSH_CUSTOM_CONFIG}/includes/zsh.java.sh
# NodeJS
#source ${ZSH_CUSTOM_CONFIG}/includes/zsh.nodejs.sh
# Fortune and Cow
#source ${ZSH_CUSTOM_CONFIG}/includes/zsh.cow-fortune.sh
