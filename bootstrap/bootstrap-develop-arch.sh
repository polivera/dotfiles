#!/bin/bash
# Exit immediately after error
set -e

currentUser=$(whoami)
dotFilesRoot=$( cd "$( dirname "$(readlink -f $0)" )/../" && pwd -P )
homeFolder=/home/$currentUser
configFolder=${homeFolder}/.config

sudo pacman -S docker docker-compose docker-machine \
        mysql-worbench  --noconfirm --needed
