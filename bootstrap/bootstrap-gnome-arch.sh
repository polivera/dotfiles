#!/bin/bash
# Exit immediately after error
set -e

currentUser=$(whoami)
dotFilesRoot=$( cd "$( dirname "$(readlink -f $0)" )/../" && pwd -P )
homeFolder=/home/$currentUser
configFolder=${homeFolder}/.config

sudo pacman -S eog evince file-roller gnome-calculator gnome-calendar \
    gnome-characters gnome-clocks gnome-color-manager gnome-control-center \
    gnome-disk-utility gnome-font-viewer gnome-documents gnome-keyring \
    gnome-logs gnome-menus gnome-photos gnome-screenshot gnome-session \
    gnome-settings-daemon gnome-shell gnome-shell-extensions \
    gnome-system-monitor gnome-terminal gnome-user-share gnome-video-effects \
    gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs \
    gvfs-smb mousetweaks mutter nautilus networkmanager rygel sushi \
    tracker tracker-miners xdg-user-dirs-gtk simple-scan evolution \
    gnome-weather gnome-tweaks chrome-gnome-shell --noconfirm --needed

wget https://aur.archlinux.org/cgit/aur.git/snapshot/pamac-aur.tar.gz
gunzip pamac-aur.tar.gz && tar -xvf pamac-aur.tar && cd pamac-aur
makepkg -si && cd .. && rm -rf pamac-aur && rm -rf pamac-aur.tar

sudo pacman -S openconnect networkmanager-openconnect libreoffice-fresh
