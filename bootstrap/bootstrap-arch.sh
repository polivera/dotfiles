#!/bin/bash
# Exit immediately after error
set -e

currentUser=$(whoami)
dotFilesRoot=$( cd "$( dirname "$(readlink -f $0)" )/../" && pwd -P )
homeFolder=/home/$currentUser
configFolder=${homeFolder}/.config

echo " ######### Updating to latest version"
# sudo pacman -Syyu

echo " ######### Installing nvidia drivers and lightdm with greeter"
sudo pacman -S lightdm lightdm-gtk-greeter --noconfirm --needed
sudo pacman -S nvidia nvidia-utils nvidia-settings ffnvcodec-headers libvdpau \
                              lib32-nvidia-utils --noconfirm --needed

echo " ######### Enable lightdm"
sudo systemctl enable lightdm

echo " ########## Install base-devel"
sudo pacman -S base-devel git wget curl --noconfirm --needed

echo " ########## Install some stuff @todo: comments"
sudo pacman -S htop zsh --noconfirm --needed

# Change shell and install oh-my-zsh
if [ $SHELL != "/bin/zsh" ]; then
    sudo chsh -s /bin/zsh $currentUser
fi

echo " ########## Install oh-my-zsh"
# Install oh-my-zsh
if [ ! -x $configFolder/oh-my-zsh ]; then
    git clone https://github.com/robbyrussell/oh-my-zsh.git \
        $configFolder/oh-my-zsh
    echo "source $dotFilesRoot/zsh/init.sh" > $homeFolder/.zshrc
    source $homeFolder/.zshrc
fi
