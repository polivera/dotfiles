#!/bin/bash
currentUser=$(whoami)
dotFilesRoot=$( cd "$( dirname "$(readlink -f $0)" )/../" && pwd -P )
homeFolder=/home/$currentUser
configFolder=${homeFolder}/.config

echo " ########## Install desktop bro"
sudo pacman -S openbox obconf --noconfirm --needed
if [ -x $configFolder/openbox/ ]; then
        rm -rf $configFolder/openbox
fi
ln -s $dotFilesRoot/openbox $configFolder/openbox

if [ ! -x /usr/bin/polybar ]; then
    echo " ########## Downloading plybar"
    wget https://aur.archlinux.org/cgit/aur.git/snapshot/polybar.tar.gz && \
              gunzip polybar.tar.gz && tar -xvf polybar.tar && rm polybar.tar
    cd polybar && makepkg -si && cd .. && rm -rf polybar
fi
if [ -x $configFolder/polybar ]; then
    rm -rf $configFolder/polybar
fi
ln -s $dotFilesRoot/polybar $configFolder/polybar

echo " ########## Install visual stuff @todo: also explain"
sudo pacman -S termite chromium rofi tint2 xclip \
    pcmanfm-gtk3 pavucontrol xcompmgr transset-df feh \
    imagemagick --noconfirm --needed

echo " ########## Creating links for apps"
rm $configFolder/termite && ln -s $dotFilesRoot/termite $configFolder/termite
rm $configFolder/tint2 && ln -s $dotFilesRoot/tint2 $configFolder/tint2
